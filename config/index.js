/* eslint-disable no-magic-numbers */
import localConfig from './local';

const config = {
  posterBaseUrl: 'https://image.tmdb.org/t/p/w300_and_h450_bestv2',
  endpoints: {
    searchMulti: '/search/multi',
  },
  mediaTypes: {
    movie: 'movie',
    person: 'person',
    tv: 'tv',
  },
  productionOverviewMaxLength: 300,
};

export default { ...config, ...localConfig };
