export default (modules, container) => modules.forEach(module => module.init(container));
