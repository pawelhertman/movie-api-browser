export default class View {
  constructor (element, template) {
    this.element = element;
    this.template = template;
  }

  render (modelView) {
    this.element.innerHTML = this.template(modelView);

    this._bindEventListeners();
  }

  _bindEventListeners () {
    return null;
  }
}
