export default class Controller {
  constructor (view) {
    this.view = view;
    this.initialModelView = {};
  }

  initialize () {
    // bind view's event listeners
    this.view.render(this.initialModelView);
  }
}
