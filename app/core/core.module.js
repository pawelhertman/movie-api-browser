import Module from './module';

import ApiService from './services/api.service';
import MovieRepository from './services/movie-repository.service';

export default class SearchModule extends Module {
  static init (container) {
    this._initServices(container);
  }

  static _initServices (container) {
    const apiService = new ApiService();
    container.add('api-service', apiService);
    container.add('movie-repository', new MovieRepository(apiService));
  }
}
