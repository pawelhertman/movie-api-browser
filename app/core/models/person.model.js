import config from '../../../config/index';

import MediaResult from './media-result.model';
import Movie from './movie.model';
import TvShow from './tv-show.model';

const PRODUCTIONS_NAMES_BINDER = ', ';

export default class Production extends MediaResult {
  constructor (apiData) {
    super(apiData);

    this.name = apiData.name;
    this.imageUrl = (apiData.profile_path) ? `${config.posterBaseUrl}${apiData.profile_path}` : null;
    this.knownFor = apiData.known_for.map(
      productionApiData => productionApiData === config.mediaTypes.tv
        ? new TvShow(productionApiData)
        : new Movie(productionApiData)
    );
  }

  getProductionsNames () {
    return this.knownFor
      .filter(production => production.name) // not every production has a name
      .map(production => production.name)
      .join(PRODUCTIONS_NAMES_BINDER);
  }
}
