import Production from './production.model';

export default class TvShow extends Production {
  constructor (apiData) {
    super(apiData);

    this.name = apiData.name;
    this.date = apiData.first_air_date ? new Date(apiData.first_air_date) : null;
  }
}
