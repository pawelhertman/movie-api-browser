export default class MediaResult {
  constructor (apiData) {
    this.id = apiData.id;
    this.date = null;
  }

  getDateYear () {
    return (this.date instanceof Date)
      ? this.date.getFullYear()
      : '';
  }
}
