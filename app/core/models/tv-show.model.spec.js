/* eslint-disable no-magic-numbers */
import { EXAMPLE_TV_SHOW } from '../../../spec/helpers/fixtures';
import config from '../../../config/index';

import TvShow from './tv-show.model';

describe('TvShow Model', () => {
  it('should be created with proper properties', function () {
    const show = new TvShow(EXAMPLE_TV_SHOW);

    expect(show.id).toBe(62413);
    expect(show.name).toBe('Killjoys');
    expect(show.voteCount).toBe(124);
    expect(show.voteAverage).toBe(6.56);
    expect(show.imageUrl).toBe(`${config.posterBaseUrl}/oE8xODVOifddrJyDOvk8dnA3Wzs.jpg`);
    expect(show.date).toEqual(new Date('2015-06-19'));
    // eslint-disable-next-line max-len
    expect(show.overview).toBe('An action-packed adventure series following a fun-loving, hard living trio of interplanetary bounty hunters (a.k.a. Killjoys) sworn to remain impartial as they chase deadly warrants around the Quad, a system of planets on the brink of revolution.');
  });
});
