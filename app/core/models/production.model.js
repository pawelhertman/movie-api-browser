import config from '../../../config/index';

import MediaResult from './media-result.model';

export default class Production extends MediaResult {
  constructor (apiData) {
    super(apiData);

    this.imageUrl = (apiData.poster_path) ? `${config.posterBaseUrl}${apiData.poster_path}` : null;
    this.voteAverage = apiData.vote_average;
    this.voteCount = apiData.vote_count;
    this.overview = apiData.overview;
  }
}
