import Production from './production.model';

export default class Movie extends Production {
  constructor (apiData) {
    super(apiData);

    this.name = apiData.title;
    this.date = apiData.release_date ? new Date(apiData.release_date) : null;
  }
}
