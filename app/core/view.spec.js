import View from './view';

describe('Core View class', () => {
  beforeEach(function () {
    this.template = viewModel => '<p>Test html</p>';
    this.element = {
      innerHTML: null,
    };

    this.view = new View(this.element, this.template);

    this.view.render({});
  });

  it('should insert html into assigned element', function () {
    expect(this.view.element.innerHTML.indexOf('Test html')).toBeGreaterThan(0);
  });
});
