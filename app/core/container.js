export default class Container {
  constructor () {
    this.services = {};
  }

  add (name, service) {
    if (Object.keys(this.services).indexOf(name) > -1) {
      throw new Error('Already registered service with given name');
    }

    this.services[name] = service;
  }

  get (name) {
    if (Object.keys(this.services).indexOf(name) === -1) {
      throw new Error('No service with given name');
    }

    return this.services[name];
  }
}
