import config from '../../../config/local';

import ApiService from './api.service';

describe('Api Service', () => {
  beforeAll(function () {
    global.fetch = jasmine.createSpy()
      .and
      .callFake(() => Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ test: 'data' }),
      }));
    this.apiService = new ApiService();
  });

  it('Should call fetch() with correct api key', async function () {
    const REQUEST_PATH = '/search/multi-search';
    await this.apiService.get(REQUEST_PATH, {});

    expect(fetch).toHaveBeenCalledWith(
      `${config.apiUrl}${REQUEST_PATH}?api_key=${config.apiKey}`,
      { method: 'GET' }
    );
  });

  it('Should call fetch() with correct query params and api key', async function () {
    const REQUEST_PATH = '/search/multi-search';
    await this.apiService.get(REQUEST_PATH, { testParam: 'testValue' });

    expect(fetch).toHaveBeenCalledWith(
      `${config.apiUrl}${REQUEST_PATH}?testParam=testValue&api_key=${config.apiKey}`,
      { method: 'GET' }
    );
  });

  it('Should return data', async function () {
    const REQUEST_PATH = '/search/multi-search';
    const result = await this.apiService.get(REQUEST_PATH, { testParam: 'testValue' });

    expect(result).toEqual({ test: 'data' });
  });

  describe('Error path', () => {
    beforeAll(function () {
      global.fetch = jasmine.createSpy()
        .and
        .callFake(() => Promise.resolve({
          ok: false,
          json: () => Promise.resolve(new Error('response error')),
        }));
      this.apiService = new ApiService();
    });

    it('Should return data', async function () {
      const REQUEST_PATH = '/search/multi-search';
      let result;
      try {
        result = await this.apiService.get(REQUEST_PATH, {});
      } catch (err) {
        result = err.message;
      }

      expect(result).toBe('response error');
    });
  });
});
