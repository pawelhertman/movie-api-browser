/* eslint-disable no-magic-numbers */
import { EXAMPLE_API_DATA } from '../../../spec/helpers/fixtures';
import config from '../../../config';

import MovieRepository from './movie-repository.service';
import Movie from '../models/movie.model';
import TvShow from '../models/tv-show.model';
import Person from '../models/person.model';

describe('Movie Repository', () => {
  beforeEach(function () {
    this.apiService = {
      get: null,
    };
    spyOn(this.apiService, 'get')
      .and
      .callFake(() => Promise.resolve(EXAMPLE_API_DATA));

    this.movieRepository = new MovieRepository(this.apiService);
  });

  it('should call apiService with proper params', async function () {
    await this.movieRepository.searchMulti('kil', 5);

    expect(this.apiService.get)
      .toHaveBeenCalledWith(
        config.endpoints.searchMulti,
        { query: 'kil', page: 5 }
      );
  });

  it('should call apiService with page=1 by default', async function () {
    await this.movieRepository.searchMulti('querrrry');

    expect(this.apiService.get)
      .toHaveBeenCalledWith(
        config.endpoints.searchMulti,
        { query: 'querrrry', page: 1 }
      );
  });

  it('should return created models', async function () {
    const apiData = await this.movieRepository.searchMulti('querrrry');
    const results = apiData.results;

    expect(results[0] instanceof TvShow).toBe(true);
    expect(results[1] instanceof Movie).toBe(true);
    expect(results[2] instanceof Person).toBe(true);
  });
});
