import config from '../../../config/local';

const QUERY_PARAMS_JOIN_SIGN = '&';

export default class ApiService {
  async get (path, params) {
    const requestOptions = { method: 'GET' };

    const queryParams = { ...params, api_key: config.apiKey };
    const queryParamsString = Object.keys(queryParams)
      .map(key => `${key}=${queryParams[key]}`)
      .join(QUERY_PARAMS_JOIN_SIGN);

    // @todo switch to XMLHttpRequest, because Fetch API gives no possibility to cancel request
    const response = await fetch(`${config.apiUrl}${path}?${queryParamsString}`, requestOptions);

    await this._throwErrorIfFailure(response);

    return response.json();
  }

  async _throwErrorIfFailure (response) {
    if (!response.ok) {
      throw await response.json();
    }
  }
}
