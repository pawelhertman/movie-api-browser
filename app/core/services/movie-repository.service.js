import config from '../../../config';

import Movie from '../models/movie.model';
import TvShow from '../models/tv-show.model';
import Person from '../models/person.model';

export default class MovieRepository {
  constructor (apiService) {
    this.apiService = apiService;
  }

  async searchMulti (query, page = 1) {
    const apiData = await this.apiService.get(config.endpoints.searchMulti, { query, page });
    const data = { ...apiData };

    data.results = data.results.map((mediaResult) => {
      switch (mediaResult.media_type) {
        case config.mediaTypes.tv:
          return new TvShow(mediaResult);
        case config.mediaTypes.movie:
          return new Movie(mediaResult);
        case config.mediaTypes.person:
          return new Person(mediaResult);
        default:
          throw new Error(`Unknown media type: ${mediaResult.media_type}`);
      }
    });

    return data;
  }
}
