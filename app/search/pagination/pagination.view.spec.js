import PaginationView from './pagination.view';
import paginationTemplate from './pagination.template';

describe('Pagination View', () => {
  beforeEach(function () {
    this.element = {
      innerHTML: null,
      querySelector: () => null,
    };
    this.view = new PaginationView(this.element, paginationTemplate);
  });

  it('should be empty if visible === false', function () {
    this.view.render({
      visible: false,
    });

    expect(this.view.element.innerHTML).toBe('');
  });

  it('should be visible if visible === true', function () {
    this.view.render({
      visible: true,
    });

    expect(this.view.element.innerHTML.length).toBeGreaterThan(0);
  });

  it('should display "1 of 4" text', function () {
    this.view.render({
      visible: true,
      currentPage: 1,
      totalPages: 4,
    });

    expect(this.view.element.innerHTML.indexOf('1 of 4')).toBeGreaterThan(0);
  });

  it('should display "5 of 5" text', function () {
    this.view.render({
      visible: true,
      currentPage: 5,
      totalPages: 5,
    });

    expect(this.view.element.innerHTML.indexOf('5 of 5')).toBeGreaterThan(0);
  });

  it('should disable "previous" button by default', function () {
    this.view.render({
      visible: true,
    });

    expect(this.view.element.innerHTML.indexOf('previous disabled')).toBeGreaterThan(0);
  });

  it('should enable "previous" button when previousButtonDisabled === false', function () {
    this.view.render({
      visible: true,
      previousButtonDisabled: false,
    });

    expect(this.view.element.innerHTML.indexOf('previous disabled')).toBe(-1);
  });

  it('should disable "next" button by default', function () {
    this.view.render({
      visible: true,
    });

    expect(this.view.element.innerHTML.indexOf('next disabled')).toBeGreaterThan(0);
  });

  it('should enable "next" button when nextButtonDisabled === false', function () {
    this.view.render({
      visible: true,
      nextButtonDisabled: false,
    });

    expect(this.view.element.innerHTML.indexOf('next disabled')).toBe(-1);
  });
});
