import View from '../../core/view';

const PREVIOUS_BUTTON_SELECTOR = '.previous:not(.disabled) #previous-page';
const NEXT_BUTTON_SELECTOR = '.next:not(.disabled) #next-page';

export default class PaginationView extends View {
  constructor (element, template) {
    super(element, template);

    this.onPreviousPageClick = null;
    this.onNextPageClick = null;
  }

  _bindEventListeners () {
    const previousPageButton = this.element.querySelector(PREVIOUS_BUTTON_SELECTOR);
    if (previousPageButton) {
      previousPageButton.addEventListener('click', this.onPreviousPageClick);
    }

    const nextPageButton = this.element.querySelector(NEXT_BUTTON_SELECTOR);
    if (nextPageButton) {
      nextPageButton.addEventListener('click', this.onNextPageClick);
    }
  }
}
