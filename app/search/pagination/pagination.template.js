export default ({
  visible = false,
  currentPage = 1,
  totalPages = 1,
  previousButtonDisabled = true,
  nextButtonDisabled = true,
}) => (visible === true)
  ? `
    <div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
      <ul class="pager">
        <li class="previous ${previousButtonDisabled ? 'disabled' : ''}">
          <a id="previous-page"><span aria-hidden="true">&larr;</span> Previous</a>
        </li>
        <li><span class="pagination-indicator">${currentPage} of ${totalPages}</span></li>
        <li class="next ${nextButtonDisabled ? 'disabled' : ''}">
          <a id="next-page">Next <span aria-hidden="true">&rarr;</span></a>
        </li>
      </ul>
    </div>
  `
  : '';
