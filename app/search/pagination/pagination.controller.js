import Controller from '../../core/controller';

export default class PagiationController extends Controller {
  constructor (view, searcher) {
    super(view);

    this.searcher = searcher;
  }

  initialize () {
    this.view.onPreviousPageClick = e => this.onPreviousPageButtonClick(e);
    this.view.onNextPageClick = e => this.onNextPageButtonClick(e);

    super.initialize();

    this.searcher.data$.subscribe(data => this.onDataChange(data));
  }

  onPreviousPageButtonClick () {
    this.searcher.previousPage();
  }

  onNextPageButtonClick () {
    this.searcher.nextPage();
  }

  onDataChange (data) {
    const modelView = this.calculateModelView(data);
    this.view.render(modelView);
  }

  calculateModelView (data) {
    if (data.totalResults === 0) {
      return { visible: false };
    }
    if (data.totalPages === 1) {
      return { visible: true };
    }

    return {
      visible: true,
      currentPage: data.currentPage,
      totalPages: data.totalPages,
      previousButtonDisabled: data.currentPage === 1,
      nextButtonDisabled: data.currentPage === data.totalPages,
    };
  }
}
