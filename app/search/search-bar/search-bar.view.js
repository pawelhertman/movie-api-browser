import View from '../../core/view';

const INPUT_SELECTOR = '#search-input';

export default class SearchBarView extends View {
  constructor (element, template) {
    super(element, template);

    this.onInput = null;
  }

  _bindEventListeners () {
    this.element.querySelector(INPUT_SELECTOR).addEventListener('input', this.onInput);
  }
}
