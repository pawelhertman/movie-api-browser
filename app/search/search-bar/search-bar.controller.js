import Controller from '../../core/controller';

export default class SearchBarController extends Controller {
  constructor (view, searcher) {
    super(view);

    this.searcher = searcher;
  }

  initialize () {
    this.view.onInput = e => this.onSearchInputChange(e);

    super.initialize();
  }

  onSearchInputChange (e) {
    const query = e.target.value;

    this.searcher.search(query);
  }
}

