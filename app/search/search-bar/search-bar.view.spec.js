import SearchBarView from './search-bar.view';
import searchBarTemplate from './search-bar.template';

describe('Search Bar View', () => {
  beforeAll(function () {
    this.testHtml = searchBarTemplate({});
  });

  beforeEach(function () {
    this.input = {
      addEventListener: () => null,
    };
    this.element = {
      innerHTML: null,
      querySelector: (selector) => {
        switch (selector) {
          case '#search-input':
            return this.input;
          default:
            return null;
        }
      },
    };
    spyOn(this.input, 'addEventListener');

    this.view = new SearchBarView(this.element, searchBarTemplate);
  });

  it('should render header', function () {
    this.view.render({});

    expect(this.view.element.innerHTML.indexOf('<h1>The Movie DB browser</h1>'))
      .toBeGreaterThan(0);
  });

  it('should render input', function () {
    this.view.render({});

    expect(this.view.element.innerHTML.indexOf('<input type="text"'))
      .toBeGreaterThan(0);
  });

  it('should render same HTML with not empty view model', function () {
    this.view.render({
      testProp: 'test',
    });

    expect(this.view.element.innerHTML).toBe(this.testHtml);
  });

  it('should add input event listener', function () {
    this.view.render({});

    expect(this.input.addEventListener).toHaveBeenCalledWith('input', null);
  });
});
