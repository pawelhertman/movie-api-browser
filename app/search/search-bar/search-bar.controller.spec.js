import SearchBarController from './search-bar.controller';

describe('Search Bar Controller', () => {
  beforeEach(function () {
    this.view = {
      onInput: null,
      render: () => null,
    };
    this.controller = new SearchBarController(this.view);
    this.controller.initialize();
  });

  it('should catch input change event', function () {
    spyOn(this.controller, 'onSearchInputChange');
    this.view.onInput();

    expect(this.controller.onSearchInputChange).toHaveBeenCalled();
  });
});
