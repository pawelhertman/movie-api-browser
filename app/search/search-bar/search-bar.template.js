export default modelView => `
  <div class="jumbotron col-xs-12 text-center">
    <h1>The Movie DB browser</h1>
    <div class="form-group row">
      <div class="col-xs-12 col-lg-offset-2 col-lg-8">
        <input type="text" id="search-input" class="form-control" placeholder="Type something to search...">
      </div>
    </div>
  </div>
`;
