export default class ResultsStorage {
  constructor (pagesCounts, totalResults) {
    this.pagesCount = pagesCounts;
    this.totalResults = totalResults;
    this.pages = {};
  }

  /**
   * Returns results from page with given number
   * @param {number} pageNumber
   * @throws Throws an error if given pageNumber is bigger than pages count
   * @returns {(Person|Movie|TvShow)[]|null}
   */
  getPage (pageNumber) {
    if (pageNumber > this.pagesCount) {
      throw new Error('Index out of pages count');
    }

    if (!this.pages[pageNumber]) {
      return null;
    }

    return this.pages[pageNumber];
  }

  /**
   * Sets page's results
   * @param {number} pageNumber
   * @param {(Person|Movie|TvShow)[]} results
   * @throws Throws an error if given pageNumber is bigger than pages count
   * @returns {(Person|Movie|TvShow)[]|null}
   */
  setPage (pageNumber, results) {
    if (pageNumber > this.pagesCount) {
      throw new Error('Index out of pages count');
    }

    if (!Array.isArray(results) || !results.length) {
      return null;
    }

    this.pages[pageNumber] = [ ...results ];

    return this.pages[pageNumber];
  }
}
