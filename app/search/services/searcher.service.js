import Observable from '../../utils/observable';
import ResultsStorage from './results-storage';

// @todo create unit tests
export default class Searcher {
  constructor (movieRepository) {
    this.movieRepository = movieRepository;
    this.storage = null;
    this.data$ = new Observable();
    this.currentData = null;
    this.currentQuery = null;
  }

  /**
   * Runs fetching results by query
   * @param query
   * @returns {Promise.<*>}
   */
  async search (query) {
    if (query === '' || query === this.currentQuery) {
      return;
    }

    // @todo cancel current fetch if in progress
    const data = await this.movieRepository.searchMulti(query);
    this.currentQuery = query;

    this.storage = new ResultsStorage(data.total_pages, data.total_results);
    this.storage.setPage(1, data.results);

    return this._updateDataStream(1, data.results);
  }

  /**
   * Switches to the next page if possible
   * @returns {*}
   */
  nextPage () {
    if (this.currentData.currentPage === this.currentData.totalPages) {
      return Promise.resolve(null);
    }

    return this._changePage(this.currentData.currentPage + 1);
  }

  /**
   * Switches to the next page if possible
   * @returns {*}
   */
  previousPage () {
    if (this.currentData.currentPage === 1) {
      return Promise.resolve(null);
    }

    return this._changePage(this.currentData.currentPage - 1);
  }

  /**
   * Changes page and runs fetching a page
   * @param pageNumber
   * @returns {Promise.<*>}
   * @private
   */
  async _changePage (pageNumber) {
    let items = this.storage.getPage(pageNumber);

    if (!items) {
      // @todo cancel current fetch if in progress
      const data = await this.movieRepository.searchMulti(this.currentQuery, pageNumber);
      this.storage.setPage(pageNumber, data.results);

      items = data.results;
    }

    return this._updateDataStream(pageNumber, items);
  }

  _updateDataStream (pageNumber, items) {
    const data = {
      currentPage: pageNumber,
      totalPages: this.storage.pagesCount,
      totalResults: this.storage.totalResults,
      items: items,
    };

    this.data$.notify(data);
    this.currentData = data;

    return data;
  }
}
