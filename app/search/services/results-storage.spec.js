/* eslint-disable no-magic-numbers */

import ResultsStorage from './results-storage';

const STORAGE_PAGES_COUNT = 4;
const EXAMPLE_PAGE_RESULTS = ['record1', 'record2'];

describe('ResultsStorage', function () {
  beforeAll(function () {
    this.storage = new ResultsStorage(STORAGE_PAGES_COUNT);
  });

  it('should correctly set pages count on create', function () {
    expect(this.storage.pagesCount).toBe(STORAGE_PAGES_COUNT);
  });

  it('should create an empty pages object on create', function () {
    expect(this.storage.pages).toEqual({});
  });

  it('should return null when getting not set page', function () {
    expect(this.storage.getPage(1)).toBe(null);
  });

  it('should correctly set page and return results', function () {
    expect(this.storage.setPage(1, EXAMPLE_PAGE_RESULTS)).toEqual(EXAMPLE_PAGE_RESULTS);
    expect(this.storage.setPage(STORAGE_PAGES_COUNT, EXAMPLE_PAGE_RESULTS)).toEqual(EXAMPLE_PAGE_RESULTS);
  });

  it('should return proper page results', function () {
    expect(this.storage.getPage(1)).toEqual(EXAMPLE_PAGE_RESULTS);
    expect(this.storage.getPage(STORAGE_PAGES_COUNT)).toEqual(EXAMPLE_PAGE_RESULTS);
  });

  it('should omit setting page when trying to set something else than not empty array', function () {
    expect(this.storage.setPage(1, null)).toBe(null);
    expect(this.storage.setPage(1, [])).toBe(null);
    expect(this.storage.setPage(1, 'test')).toBe(null);
  });

  it('should throw an exception when trying to get page with index bigger than pagesCount', function () {
    expect(() => this.storage.getPage(STORAGE_PAGES_COUNT + 1)).toThrowError(Error, 'Index out of pages count');
  });

  it('should throw an exception when trying to set page with index bigger than pagesCount', function () {
    expect(() => this.storage.setPage(STORAGE_PAGES_COUNT + 1, ['any']))
      .toThrowError(Error, 'Index out of pages count');
  });
});
