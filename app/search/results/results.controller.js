import Controller from '../../core/controller';

import './results.scss';

export default class ResultsController extends Controller {
  constructor (view, searcher) {
    super(view);

    this.searcher = searcher;
  }

  initialize () {
    super.initialize();

    this.searcher.data$.subscribe(data => this.onDataChange(data));
  }

  onDataChange (data) {
    this.view.render({
      results: data.items,
    });

    window.scrollTo(0, 0);
  }
}
