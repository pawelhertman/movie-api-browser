import Movie from '../../core/models/movie.model';
import Person from '../../core/models/person.model';
import TvShow from '../../core/models/tv-show.model';

import productionTemplate from './templates/production.template';
import personTemplate from './templates/person.template';

export default (modelView) => {
  const content = (!modelView.results || !modelView.results.length)
    ? '<p class="col-xs-12 alert alert-info text-center" role="alert">No results</p>'
    : modelView.results.map(result => getResultHtml(result)).join('');

  return `
    <div class="row">${content}</div>
  `;
};

const getResultHtml = (mediaResult) => {
  switch (mediaResult.constructor) {
    case Movie:
    case TvShow:
      return productionTemplate(mediaResult);
    case Person:
      return personTemplate(mediaResult);
  }

  throw new Error(`Unknown result type: ${mediaResult.type}`);
};
