/* eslint-disable no-magic-numbers */
import { getStringOccurrencesCount } from '../../../spec/helpers/expect';
import { EXAMPLE_PERSON, EXAMPLE_MOVIE, EXAMPLE_TV_SHOW } from '../../../spec/helpers/fixtures';

import ResultsView from './results.view';
import resultsTemplate from './results.template';

import Movie from '../../core/models/movie.model';
import Person from '../../core/models/person.model';
import TvShow from '../../core/models/tv-show.model';

describe('Results View', () => {
  beforeEach(function () {
    this.element = {
      innerHTML: null,
    };
    this.view = new ResultsView(this.element, resultsTemplate);
  });

  it('should display movie, person and TV Show', function () {
    this.view.render({
      results: [
        new Movie(EXAMPLE_MOVIE),
        new Person(EXAMPLE_PERSON),
        new TvShow(EXAMPLE_TV_SHOW),
      ],
    });

    expect(getStringOccurrencesCount(this.view.element.innerHTML, 'result--production')).toBe(2);
    expect(getStringOccurrencesCount(this.view.element.innerHTML, 'result--person')).toBe(1);
  });

  it('should display info when no results', function () {
    this.view.render({
      results: [],
    });

    expect(this.view.element.innerHTML.indexOf('No results')).toBeGreaterThan(0);
  });

  it('should display info when "results" is undefined', function () {
    this.view.render({});

    expect(this.view.element.innerHTML.indexOf('No results')).toBeGreaterThan(0);
  });
});
