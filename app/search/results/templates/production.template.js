import config from '../../../../config';

export default production => `
  <article class="col-xs-12 col-sm-4 col-lg-3 result result--production">
    <img src="${production.imageUrl || ''}" class="result__img" />
    <h2 class="result__title">${production.name}</h2>
    <div class="result__details">
      <h3>Vote: ${production.voteAverage} (${production.voteCount} votes)</h3>
      <p><strong>Year:</strong> ${production.getDateYear() || 'unknown'}</p>
      <p>${production.overview.slice(1, config.productionOverviewMaxLength)}</p>
    </div>
  </article>
`;
