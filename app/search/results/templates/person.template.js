export default person => `
  <article class="col-xs-12 col-sm-4 col-lg-3 result result--person">
    <img src="${person.imageUrl || ''}" class="result__img" />
    <h2 class="result__title">${person.name}</h2>
    <div class="result__details">
      <p><strong>Year:</strong> ${person.getDateYear() || 'unknown'}</p>
      <p><strong>Known For:</strong> ${person.getProductionsNames()}</p>
    </div>
  </article>
`;
