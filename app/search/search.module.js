import Module from '../core/module';

import Searcher from './services/searcher.service';

import SearchBarController from './search-bar/search-bar.controller';
import searchBarTemplate from './search-bar/search-bar.template';
import SearchBarView from './search-bar/search-bar.view';

import ResultsController from './results/results.controller';
import resultsTemplate from './results/results.template';
import ResultsView from './results/results.view';

import PaginationController from './pagination/pagination.controller';
import paginationTemplate from './pagination/pagination.template';
import PaginationView from './pagination/pagination.view';

export default class SearchModule extends Module {
  static init (container) {
    this._initServices(container);

    this._initSearchBar(container);
    this._initResults(container);
    this._initPagination(container);
  }

  static _initServices (container) {
    container.add('searcher', new Searcher(container.get('movie-repository')));
  }

  static _initSearchBar (container) {
    const element = document.getElementById('search-bar');
    const view = new SearchBarView(element, searchBarTemplate);
    const controller = new SearchBarController(
      view,
      container.get('searcher')
    );

    controller.initialize();
  }

  static _initResults (container) {
    const element = document.getElementById('results');
    const view = new ResultsView(element, resultsTemplate);
    const controller = new ResultsController(
      view,
      container.get('searcher')
    );

    controller.initialize();
  }

  static _initPagination (container) {
    const element = document.getElementById('pagination');
    const view = new PaginationView(element, paginationTemplate);
    const controller = new PaginationController(
      view,
      container.get('searcher')
    );

    controller.initialize();
  }
}
