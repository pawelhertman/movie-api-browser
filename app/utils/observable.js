export default class Observable {
  constructor () {
    this.subscribers = [];
  }

  notify (value) {
    this.subscribers.forEach(subscriber => subscriber(value));
  }

  subscribe (subscriber) {
    if (this.subscribers.indexOf(subscriber) > -1) {
      return;
    }

    this.subscribers.push(subscriber);
  }

  unsubscribe (subscriber) {
    const subscriberIndex = this.subscribers.indexOf(subscriber);

    if (subscriberIndex === -1) {
      throw new Error('Not registered as a subscriber');
    }

    this.subscribers.splice(subscriberIndex, 1);
  }
}
