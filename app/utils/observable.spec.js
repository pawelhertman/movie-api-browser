/* eslint-disable no-magic-numbers */

import Observable from './observable';

describe('Observable', () => {
  beforeAll(function () {
    this.observable = new Observable();
    this.subscriber1 = null;
    this.subscriber2 = null;

    spyOn(this, 'subscriber1')
      .and
      .callFake(value => 1);
    spyOn(this, 'subscriber2')
      .and
      .callFake(value => 2);
  });

  it('should have empty array of subscribers on create', function () {
    expect(this.observable.subscribers).toEqual([]);
  });

  it('should correctly add a subscriber', function () {
    this.observable.subscribe(this.subscriber1);

    expect(this.observable.subscribers.indexOf(this.subscriber1)).toBeGreaterThan(-1);
  });

  it('should omit subscribing when trying to subscribe another time', function () {
    this.observable.subscribe(this.subscriber1);

    expect(this.observable.subscribers.length).toBe(1);
  });

  it('should correctly add another subscriber', function () {
    this.observable.subscribe(this.subscriber2);

    expect(this.observable.subscribers.indexOf(this.subscriber2)).toBeGreaterThan(-1);
    expect(this.observable.subscribers.indexOf(this.subscriber1)).toBeGreaterThan(-1);
  });

  it('should correctly notify all subscribers', function () {
    this.observable.notify('test value');

    expect(this.subscriber1).toHaveBeenCalledWith('test value');
    expect(this.subscriber2).toHaveBeenCalledWith('test value');
  });

  it('should correctly remove a subscriber', function () {
    this.observable.unsubscribe(this.subscriber1);

    expect(this.observable.subscribers.indexOf(this.subscriber1)).toBe(-1);
    expect(this.observable.subscribers.indexOf(this.subscriber2)).toBeGreaterThan(-1);
  });

  it('should throw an error when trying to remove not existed subscriber', function () {
    expect(() => this.observable.unsubscribe(this.subscriber1)).toThrowError(Error, 'Not registered as a subscriber');
  });

  it('should correctly notify about another value', function () {
    this.observable.notify('another value');

    expect(this.subscriber2).toHaveBeenCalledWith('another value');
    expect(this.subscriber2).toHaveBeenCalledTimes(2);
    expect(this.subscriber1).toHaveBeenCalledTimes(1);
  });
});
