import 'styles/index.scss';

import Container from './core/container';
import bootstrapModules from 'bootstrap/modules';
import CoreModule from './core/core.module';
import SearchModule from './search/search.module';

const container = new Container();

bootstrapModules([
  CoreModule,
  SearchModule,
], container);
