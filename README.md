The Movie Database browser
===========

# Requirements
You only need <b>node.js</b> pre-installed and you’re good to go. 

## Setup
Install dependencies
```sh
$ npm install
```

Copy local config file and provide missing data in config/local.js
```sh
$ cp config/local.js.dist config/local.js
```

## Development

Run the local webpack-dev-server with livereload and autocompile on [http://localhost:8080/](http://localhost:8080/)
```sh
$ npm run dev
```

## @todos, remarks and known issues
- Some files don't have unit tests due to lack of time. All of the most important/representatives
(except Searcher service) objects are covered with unit tests.
- There are a few @todos in code due to lack of time
- Only example JSDoc comments has been created due to lack of time. Anyway, in my opinion, there is no need for 
additional documentation of this code - it's **self-documented**.
- Loading indicator has to be implemented while fetching results
- Tiles require visual styles polishing, especially for mobile devices

## Deployment
Build the current application
```sh
$ npm run build
```
