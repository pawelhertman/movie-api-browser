export const EXAMPLE_MOVIE = {
  vote_average: 0,
  vote_count: 0,
  id: 203854,
  media_type: 'movie',
  title: 'KIL',
  poster_path: '/cHm8AkB2oR7olaaBHFeJ6EcAhqR.jpg',
  // eslint-disable-next-line max-len
  overview: 'kil is a depressed young man who has suicidal tendencies but he could never bring himself to commit suicide. His attempts to die always fail miserably until he finds the perfect solution: a quirky assassination agency that helps kill suicidal people. The assassination will be anonymous. He would not know the details about who the assassin is, even when or where he will die. This was ideal for Akil, until he falls for a girl he meets, Zara. Suddenly, he starts to have second thoughts on taking his life.',
  release_date: '2013-05-29',
};

export const EXAMPLE_TV_SHOW = {
  original_name: 'Killjoys',
  id: 62413,
  media_type: 'tv',
  name: 'Killjoys',
  vote_count: 124,
  vote_average: 6.56,
  poster_path: '/oE8xODVOifddrJyDOvk8dnA3Wzs.jpg',
  first_air_date: '2015-06-19',
  // eslint-disable-next-line max-len
  overview: 'An action-packed adventure series following a fun-loving, hard living trio of interplanetary bounty hunters (a.k.a. Killjoys) sworn to remain impartial as they chase deadly warrants around the Quad, a system of planets on the brink of revolution.',
};

export const EXAMPLE_PERSON = {
  popularity: 1.000853,
  media_type: 'person',
  id: 99001,
  profile_path: null,
  name: 'Maria Kil',
  known_for: [ EXAMPLE_MOVIE, EXAMPLE_TV_SHOW ],
  adult: false,
};

export const EXAMPLE_API_DATA = {
  page: 1,
  total_results: 3,
  total_pages: 1,
  results: [ EXAMPLE_TV_SHOW, EXAMPLE_MOVIE, EXAMPLE_PERSON ],
};
