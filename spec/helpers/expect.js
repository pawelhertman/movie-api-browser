export function getStringOccurrencesCount (source, search) {
  const regexp = new RegExp(search, 'g');

  return (source.match(regexp) || []).length;
};
